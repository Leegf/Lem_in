/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/24 16:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/24 16:57:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief receives the error to be displayed and exits the program.
** @param str error. All errors are defined in the header.
*/

void			error(char *str)
{
	if (g_output)
		ft_lst_clear(&g_output);
	ft_dprintf(2, "{red}ERROR: %s{eoc}\n", str);
	exit(1);
}

/*
** @brief counts the len of the number(like strlen)
** @param num the number.
** @return return the length.
*/

int				num_len(int num)
{
	int count;

	count = 1;
	if (num < 0)
	{
		num *= -1;
		count++;
	}
	while (num / 10 >= 1)
	{
		num /= 10;
		count++;
	}
	return (count);
}

static t_list	*lstnew_l(void const *content, size_t content_size)
{
	t_list *tmp;

	tmp = (t_list*)malloc(sizeof(t_list));
	if (tmp == NULL)
		return (NULL);
	if (content == NULL)
	{
		tmp->content = NULL;
		tmp->content_size = 0;
	}
	else
	{
		tmp->content_size = content_size + 1;
		tmp->content = malloc(content_size + 1);
		tmp->content = ft_memmove(tmp->content, content, content_size);
		((char *)(tmp->content))[content_size] = '\n';
	}
	tmp->next = NULL;
	return (tmp);
}

/*
** @brief the list which is used for outputing input.
** @param tmp the pointer to the beginning of the list.
** @param data in this case it would be the string the store.
** @param con_s size of the string.
*/

void			lst_push_back_l(t_list **tmp, void *data, size_t con_s)
{
	t_list *hm;
	t_list *kek;

	if (!tmp)
		return ;
	kek = *tmp;
	hm = lstnew_l((void *)data, con_s);
	if (hm == NULL)
		return ;
	if (!(*tmp))
	{
		*tmp = hm;
		return ;
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
}

/*
** @brief made solely for the purpose of start-end connection.
** @param ants_inf basic info structure.
*/

void			two_rooms(t_ant *ants_inf)
{
	int		i;
	char	*out;

	str_from_lsts(g_output, &out);
	ft_printf("%s\n", out);
	ft_lst_clear(&g_output);
	ft_strdel(&out);
	i = 0;
	while (i < ants_inf->num)
	{
		ft_printf("%s-%s ", ants_inf->start->name, ants_inf->end->name);
		i++;
	}
	ft_printf("\n");
}
