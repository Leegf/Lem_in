This project explores the problem of finding the best routes. Algorithm used here is bfs.

To compile: `make`.

Other commands: `make clean`, `make fclean`, `make re`

Objectives:


![objective1](https://i.imgur.com/E7IKRK1.png)
![objective2](https://i.imgur.com/TixSeAv.png)












Examples of usage:


















![example](https://i.imgur.com/pTtWB0Z.png)
