/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sending_ants.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 18:00:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/09 18:00:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief once again I came up with a good explanatory name. Keep up, me.
** @param routes list of found routes.
** @return first of the shortest routes.
*/

t_route		*find_shortest_route(t_route *routes)
{
	t_route *tmp;

	tmp = routes;
	while (routes)
	{
		if (routes->size < tmp->size)
			tmp = routes;
		routes = routes->next;
	}
	return (tmp);
}

/*
** @brief fills paths of ant_band with NULL, so it'll be easier to iterate
** through it.
** @param ant_band array of ants.
** @param num number of ants.
*/

void		initialize_band(t_an ant_band[], int num)
{
	int i;

	i = 0;
	while (i < num + 1)
		ant_band[i++].path = NULL;
}

/*
** @brief makes preparations to actually send the ants: decides the path for
** each ant.
** @param routes list of found routes.
** @param room list of rooms.
** @param ants_inf basic info structure.
** @param shortest shortest path. (assume that there might be several shortest,
** paths. It contains a pointer to the first shortest).
*/

void		send_ants(t_route **routes, t_ant *ants_inf, t_route *shortest)
{
	t_an	ant_band[ants_inf->num + 1];
	size_t	counter;
	t_route	*tmp;
	int		tmp_num_of_ants;

	if (bonus_flags.s)
		return (send_via_shortest(ants_inf, shortest));
	counter = 1;
	tmp_num_of_ants = ants_inf->num;
	initialize_band(ant_band, ants_inf->num);
	while (tmp_num_of_ants)
	{
		tmp = *routes;
		while (tmp && tmp_num_of_ants)
		{
			ant_band[counter - 1].num = counter;
			if (tmp_num_of_ants)
				ant_band[counter - 1].path = tmp->links->next;
			tmp = tmp->next;
			counter++;
			tmp_num_of_ants--;
		}
	}
	process_ants(ant_band, ants_inf, 0);
}
