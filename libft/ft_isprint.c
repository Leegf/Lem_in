/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 18:44:11 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:05:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** beginng with a space and ending with a ~
*/

int		ft_isprint(int c)
{
	unsigned char tmp;

	if (c > 255 || c < 0)
		return (0);
	tmp = (unsigned char)c;
	if (tmp > 31 && tmp < 127)
		return (1);
	return (0);
}
