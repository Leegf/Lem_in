/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 20:09:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/17 20:09:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** It searches the string till the first occurrence of '%' or '\0' comes in way.
** Once it has been found, the string goes into t_list.
** The pointer of the string follows through.
*/

int		parse_simple_str(char **fmt, t_list **head, t_flags *flags)
{
	size_t i;

	i = 0;
	while (42)
	{
		if ((*fmt)[i] == '\0' || (*fmt)[i] == '%' || (*fmt)[i] == '{')
		{
			if ((*fmt)[i] == '%')
				(*flags).perc = 1;
			ft_lst_push_back(head, &((*fmt)[0]), i);
			(*fmt) += (*fmt)[i] == '\0' || (*fmt)[i] == '{' ? i : i + 1;
			if ((**fmt) == '{')
				parse_colors(fmt, head, flags);
			return (i);
		}
		i++;
	}
}

/*
** Here we make sure flags are zero-initialized. Further on depending on 1 or 0
** within in it some magic would happen.
*/

void	initialise_flags(t_flags *flags)
{
	(*flags).hash = 0;
	(*flags).zero = 0;
	(*flags).minus = 0;
	(*flags).plus = 0;
	(*flags).space = 0;
	(*flags).width = 0;
	(*flags).prec = -1;
	(*flags).length = 0;
	(*flags).conv = 0;
	(*flags).point = 0;
	(*flags).perc = 0;
	(*flags).sign = 0;
}

/*
** Parses first 5 flags(till width) into t_flags' struct.
*/

void	parse_flags(t_flags *flags, char **fmt)
{
	while ((**fmt) == '#' || (**fmt) == '0' || (**fmt) == '-' ||
			(**fmt) == '+' || (**fmt) == ' ')
	{
		if (**fmt == '#')
			(*flags).hash = 1;
		else if (**fmt == '0')
			(*flags).zero = 1;
		else if (**fmt == '-')
			(*flags).minus = 1;
		else if (**fmt == '+')
			(*flags).plus = 1;
		else if (**fmt == ' ')
			(*flags).space = 1;
		(*fmt)++;
	}
}

/*
** Parses width and precision. Whatever the outcome may be, it makes sure, it
** won't be a negative one. Probably won't crush.
*/

void	parse_width_n_prec(t_flags *flags, char **fmt, va_list ax)
{
	int	i;

	i = 0;
	while ((ft_isdigit((*fmt)[i])) || ((*fmt)[i]) == '-')
		i++;
	if (i > 0)
	{
		(*flags).width = ft_atoi(*fmt);
		if ((*flags).width < 0)
		{
			(*flags).minus = 1;
			(*flags).width = (*flags).width * (-1);
		}
		*fmt += i;
	}
	if (**fmt == '*')
		parse_star_w(fmt, flags, ax);
	parse_prec(flags, fmt, ax);
}

/*
** Just because 25 lines.
*/

void	parse_length_add(t_flags *flags, char **fmt)
{
	if (**fmt == 'h')
	{
		if ((*fmt)[1] == 'h')
		{
			(*flags).length = 1;
			(*fmt)++;
		}
		else
			(*flags).length = 2;
	}
	else if (**fmt == 'l')
	{
		if ((*fmt)[1] == 'l')
		{
			(*flags).length = 3;
			(*fmt)++;
		}
		else
			(*flags).length = 4;
	}
}
