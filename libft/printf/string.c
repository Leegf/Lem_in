/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 18:00:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/23 20:11:04 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Works a bit differently than it worked for char.
*/

void	form_width_s(t_flags flags, t_list **head, int size_of_str)
{
	char	*str;
	int		i;
	char	chr;

	i = 0;
	chr = flags.zero && !flags.minus ? '0' : ' ';
	if (flags.width < size_of_str)
		return ;
	str = ft_memalloc(flags.width - size_of_str);
	while (i < flags.width - size_of_str)
		str[i++] = chr;
	ft_lst_push_back(head, str, flags.width - size_of_str);
	ft_strdel(&str);
}

/*
** For the lack of 25 lines had to make it. Once again, it checks upon prec
** and width to make the magic become true.
*/

void	help_str_func(t_flags flags, char *c, t_list **head, int s)
{
	int		i;
	int		sp;
	char	*cp;
	char	chr;

	i = 0;
	sp = flags.width - flags.prec;
	if (flags.width - s > 0)
		sp -= flags.width - s;
	chr = flags.zero && !flags.minus ? '0' : ' ';
	cp = ft_memalloc(sp);
	while (i < sp)
		cp[i++] = chr;
	if (flags.minus)
	{
		ft_lst_push_back(head, c, flags.prec);
		ft_lst_push_back(head, cp, sp);
	}
	else
	{
		ft_lst_push_back(head, cp, sp);
		ft_lst_push_back(head, c, flags.prec);
	}
	ft_strdel(&cp);
}

/*
** Checks width and prec to make a proper string. Tries to work as strange
** as original printf does.
*/

void	form_da_st_for_lst(t_flags flags, char *c, t_list **head, int s)
{
	if (flags.prec >= 0 && flags.prec < s)
	{
		if (flags.width > flags.prec)
			help_str_func(flags, c, head, s);
		else
			ft_lst_push_back(head, c, flags.prec);
	}
	else
		ft_lst_push_back(head, c, s);
}

/*
** Counts length for wchar_t.
*/

int		ft_strlenu(wchar_t *st)
{
	int i;

	i = 0;
	while (st[i])
		i++;
	return (i);
}

/*
** Place from which our string_parsing begins.
*/

void	write_string(t_flags *flags, void *st, t_list **head)
{
	int		size;
	char	*str;

	str = st;
	if (!st)
		str = "(null)";
	size = st == NULL ? 6 : (int)ft_strlen((char *)st);
	if (st && ((*flags).length == 4 || (char)(*flags).conv == 'S'))
	{
		size = ft_strlenu((wchar_t *)st);
		write_l_string(flags, (wchar_t *)st, head, size);
		return ;
	}
	if ((*flags).minus)
	{
		form_da_st_for_lst(*flags, st == NULL ? str : (char *)st, head, size);
		form_width_s((*flags), head, size);
	}
	else
	{
		form_width_s((*flags), head, size);
		form_da_st_for_lst(*flags, st == NULL ? str : (char *)st, head, size);
	}
}
