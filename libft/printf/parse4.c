/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 15:09:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 15:09:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	parse_colors2(const t_colors colors[17], char **fmt, t_list **head)
{
	int		j;
	size_t	tmp;

	j = 0;
	while (j <= 17)
	{
		tmp = ft_strlen(colors[j].s1);
		if (ft_strnequ((*fmt), colors[j].s1, tmp))
		{
			*fmt += tmp;
			ft_lst_push_back(head, colors[j].s2, j == 17 ? 4 : 5);
			return ;
		}
		j++;
	}
	*fmt += 1;
	ft_lst_push_back(head, "{", 1);
}

void	parse_colors(char **fmt, t_list **head, t_flags *flags)
{
	const static t_colors colors[] = {
			{"{def}", DEFAULT},
			{"{black}", BK},
			{"{red}", RED},
			{"{green}", GR},
			{"{yellow}", YW},
			{"{blue}", BL},
			{"{magenta}", MG},
			{"{cyan}", CN},
			{"{light_gray}", LGR},
			{"{dark_gray}", DG},
			{"{light_red}", LR},
			{"{light_green}", LG},
			{"{light_yellow}", LY},
			{"{light_blue}", LB},
			{"{light_magenta}", LM},
			{"{light_cyan}", LC},
			{"{white}", WHT},
			{"{eoc}", NM},
	};

	parse_colors2(colors, fmt, head);
	parse_simple_str(fmt, head, flags);
}

void	parse_star_w(char **fmt, t_flags *flags, va_list ax)
{
	int i;

	i = 0;
	while ((*fmt)[i] == '*')
		i++;
	*fmt += i;
	(*flags).width = va_arg(ax, int);
	if ((*flags).width < 0)
	{
		(*flags).width *= -1;
		(*flags).minus = 1;
	}
	if (**fmt == '-' || ft_isdigit(**fmt))
		parse_width_n_prec(flags, fmt, ax);
}

void	parse_star_p(char **fmt, t_flags *flags, va_list ax)
{
	int i;

	i = 0;
	while ((*fmt)[i] == '*')
		i++;
	(*flags).prec = va_arg(ax, int);
	if ((*flags).prec <= -1)
		(*flags).prec = (-1);
	*fmt += i;
	if (**fmt == '.')
		parse_prec(flags, fmt, ax);
}
