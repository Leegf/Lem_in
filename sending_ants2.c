/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sending_ants2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/10 22:21:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/10 22:21:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief when 25 lines is not enough...
** @param ant_band array of ants.
** @param ants_inf basic info structure.
** @param count used for keep tracking of ants.
** @param flag used for output.
*/

static void	norme_is_cruel(t_an ant_band[], t_ant *ants_inf, int count,
							int *flag)
{
	int tmp_count;

	tmp_count = 0;
	while (tmp_count < count && ants_inf->num && ++(*flag))
	{
		if (ant_band[tmp_count].path)
		{
			ft_printf("L%ld-%s ", ant_band[tmp_count].num,
					ant_band[tmp_count].path->room->name);
			ant_band[tmp_count].path = ant_band[tmp_count].path->next;
			if (!ant_band[tmp_count].path)
			{
				ants_inf->num--;
				tmp_count++;
				continue;
			}
		}
		tmp_count++;
	}
}

/*
** @brief don't you?
** @param flag flag.
*/

void		do_you_love_norm(int flag)
{
	if (flag)
		write(1, "\n", 1);
}

/*
** @brief processes the array which was previously filled with path for ants to
** go. Works recursively.
** @param ant_band array of ants.
** @param ants_inf basic info structure.
** @param count used for keeping track of ants.
*/

void		process_ants(t_an ant_band[], t_ant *ants_inf, int count)
{
	int tmp_count;
	int flag;

	tmp_count = 0;
	flag = 0;
	if (!ants_inf->num)
		return ;
	norme_is_cruel(ant_band, ants_inf, count, &flag);
	while (tmp_count++ < ants_inf->num_of_routes && ants_inf->num && ++flag)
	{
		if (ant_band[count].path)
		{
			ft_printf("L%ld-%s ", ant_band[count].num,
					ant_band[count].path->room->name);
			ant_band[count].path = ant_band[count].path->next;
			count++;
			if (!ant_band[count - 1].path)
			{
				ants_inf->num--;
				continue;
			}
		}
	}
	do_you_love_norm(flag);
	process_ants(ant_band, ants_inf, count);
}
