# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/06 12:02:16 by lburlach          #+#    #+#              #
#    Updated: 2018/05/13 12:38:12 by lburlach         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = lem-in
OBJ =  main.o parse.o other.o room_checking.o parse2.o link_checking.o lists.o algo.o lists2.o algo2.o free_lists.o sending_ants.o sending_ants2.o bonus.o
CC = gcc
CFLAGS = -Wall -Wextra -Werror
CURDIR = libft
HEAD = lem_in.h
INC = $(addsuffix /includes, $(CURDIR))

all: $(NAME)
	$(MAKE) -C $(CURDIR)

$(NAME): $(OBJ)
	$(MAKE) -C $(CURDIR)
	$(CC) $(CFLAGS) -I $(INC) -o $@ $(CURDIR)/$(addsuffix .a, $(CURDIR)) $^

libft.a: ololo
	$(MAKE) -C $(CURDIR)/

ololo:
	true

clean:
	rm -f $(OBJ)
	$(MAKE) -C $(CURDIR) clean

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(CURDIR) fclean

re: fclean all

$(OBJ) : %.o: %.c
	$(CC) -I $(INC) -c $(CFLAGS) $< -o $@

$(OBJ): $(HEAD)
