/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 18:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/26 18:59:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_room		*new_room(char *name, int x, int y)
{
	t_room *tmp;

	tmp = (t_room *)malloc(sizeof(t_room));
	if (tmp == NULL)
		return (NULL);
	if (name == NULL)
		tmp->name = NULL;
	else
	{
		tmp->name = malloc(ft_strlen(name));
		tmp->name = ft_memmove(tmp->name, name, ft_strlen(name));
		tmp->coord_x = x;
		tmp->coord_y = y;
	}
	tmp->visited = 0;
	tmp->used = 0;
	tmp->links = NULL;
	tmp->path = NULL;
	tmp->next = NULL;
	return (tmp);
}

/*
** @brief creates a node with given parameters and pushes it back to the end of
** the list.
** @param tmp pointer to the beginning of the list.
** @param name name of the room.
** @param x coordinate.
** @param y coordinate.
** @return created node.
*/

t_room				*push_back_to_room(t_room **tmp, char *name, int x, int y)
{
	t_room *hm;
	t_room *kek;

	if (!tmp)
		return (NULL);
	kek = *tmp;
	hm = new_room(name, x, y);
	if (hm == NULL)
		return (NULL);
	if (!(*tmp))
	{
		*tmp = hm;
		return (hm);
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
	return (hm);
}

static t_link		*new_link(t_room *link)
{
	t_link *tmp;

	tmp = (t_link *)malloc(sizeof(t_link));
	if (tmp == NULL)
		return (NULL);
	if (link == NULL)
	{
		tmp->room = NULL;
		tmp->next = NULL;
	}
	else
	{
		tmp->room = link;
		tmp->next = NULL;
	}
	return (tmp);
}

/*
** @brief pushes back link to the end of the tmp list.
** @param tmp pointer to the beginning of the list.
** @param link the thing to be added.
*/

void				push_back_link(t_link **tmp, t_room *link)
{
	t_link *hm;
	t_link *kek;

	if (!tmp)
		return ;
	kek = *tmp;
	hm = new_link(link);
	if (hm == NULL)
		return ;
	if (!(*tmp))
	{
		*tmp = hm;
		return ;
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
}
