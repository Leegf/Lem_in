/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 14:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/26 14:53:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief This takes care of start and error.
** @param ant basin info structure.
** @param flag it used for deciding when input of rooms' parameters is ended and
** linkage part began.
** @param line processed line.
*/

void	commands_management(t_ant *ant, int *flag, char *line)
{
	if (*flag)
		error(ERR_N_5);
	if (ft_strequ(line, "##start"))
	{
		if (ant->start)
			error(ERR_N_6);
		*flag = 1;
	}
	else if (ft_strequ(line, "##end"))
	{
		if (ant->end)
			error(ERR_N_7);
		*flag = 2;
	}
}

/*
** @brief depending on flag's value there might be some errors.
** @param flag flag is used in fill_link func.
*/

void	validate_links(int flag)
{
	if (flag == 0)
		error(ERR_N_14);
	if (flag == 1)
		error(ERR_N_15);
}

/*
** @brief fills connection lists for according rooms.
** @param room list of rooms.
** @param str1 first name of the room to be linked.
** @param str2 second name of the room to be linked.
** @param flag used for detecting possible errors.
*/

void	fill_link(t_room **room, char *str1, char *str2, int *flag)
{
	t_room	*tmp;
	t_room	*tmp2;

	tmp = *room;
	tmp2 = *room;
	while (tmp)
	{
		if (ft_strequ(tmp->name, str1) && ++(*flag))
		{
			while (tmp2)
			{
				if (ft_strequ(tmp2->name, str2))
				{
					if (tmp == tmp2)
						error(ERR_N_16);
					push_back_link(&(tmp->links), tmp2);
					push_back_link(&(tmp2->links), tmp);
					(*flag)++;
				}
				tmp2 = tmp2->next;
			}
		}
		tmp = tmp->next;
	}
	validate_links(*flag);
}

/*
** @brief checks linkage's parameters and fills connection list accordingly.
** @param room list of rooms.
** @param ant basic info structure.
** @param line processed line.
*/

void	check_and_fill_link(t_room **room, t_ant *ant, char *line)
{
	char	*str1;
	char	*str2;
	int		flag;

	if (line[0] == '#')
		return ;
	flag = 0;
	if (!ant->start)
		error(ERR_N_12);
	else if (!ant->end)
		error(ERR_N_13);
	str1 = retrieve_first_link_comp(line);
	str2 = retrieve_second_link_comp(line);
	if (str2[0] == '\0')
		error(ERR_N_11);
	fill_link(room, str1, str2, &flag);
	ft_strdel(&str1);
	ft_strdel(&str2);
}
