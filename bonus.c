/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 19:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/11 19:34:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief outputs paths in a neat way.
** @param routes list of routes.
** @param shortest first of the shortest routes.
*/

void	output_paths(t_route *routes, t_route *shortest)
{
	t_link	*tmp_link;
	int		flag;
	int		count;

	count = 0;
	while (routes)
	{
		flag = 0;
		if (routes == shortest)
			flag = 1;
		tmp_link = routes->links;
		while (tmp_link)
		{
			ft_printf("{green}%s{eoc}", tmp_link->room->name);
			tmp_link = tmp_link->next;
			if (tmp_link)
				ft_printf("{green}-{eoc}");
		}
		if (flag)
			ft_printf(" -- {cyan}first found shortest path{eoc}");
		ft_printf("\n");
		routes = routes->next;
	}
	ft_printf("\n");
}

/*
** @brief makes pretty much the same as send_ants func, but sends all ants via
** the shortest path and only through it.
** @param routes list of found routes.
** @param room list of rooms.
** @param ants_inf basic info structure.
** @param shortest shortest path. (assume that there might be several shortest,
** paths. It contains a pointer to the first shortest).
*/

void	send_via_shortest(t_ant *ants_inf, t_route *shortest)
{
	t_an	ant_band[ants_inf->num + 1];
	size_t	counter;
	t_route	*tmp;
	int		tmp_num_of_ants;

	counter = 1;
	tmp_num_of_ants = ants_inf->num;
	initialize_band(ant_band, ants_inf->num);
	tmp = shortest;
	ants_inf->num_of_routes = 1;
	while (tmp_num_of_ants)
	{
		ant_band[counter - 1].num = counter;
		if (tmp_num_of_ants)
			ant_band[counter - 1].path = tmp->links->next;
		counter++;
		tmp_num_of_ants--;
	}
	process_ants(ant_band, ants_inf, 0);
}
