/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 13:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/13 16:13:20 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief breadth-first search of routes.
** @param ants_inf basic info structure.
** @param room list of rooms.
** @param routes list of found routes.
** @param queue queue which is used for bfs' algo.
** @return if new route were found returns 1. Otherwise, 0.
*/

int			bfs_search(t_ant *ants_inf, t_room **room, t_route **routes,
				t_link **queue)
{
	t_link	*parent;
	t_link	*links_iterat;

	while (*queue)
	{
		if ((*queue)->room->visited && !((*queue)->room->used))
		{
			if ((*queue)->room == ants_inf->end)
				return (push_route_to_solutions(routes, queue, ants_inf, room));
			parent = (*queue)->room->path;
			links_iterat = (*queue)->room->links;
			while (links_iterat)
			{
				if (links_iterat->room->path || links_iterat->room->used)
				{
					links_iterat = links_iterat->next;
					continue;
				}
				merge_lists(links_iterat->room, parent, links_iterat, queue);
				links_iterat = links_iterat->next;
			}
		}
		pop_queue(queue);
	}
	return (0);
}

/*
** @brief prep for bfs search and handling some of the errors.
** @param room list of rooms.
** @param ants_inf basic info structure.
** @return it returns routes that were found in case there were some.
*/

t_route		*find_routes(t_room **room, t_ant *ants_inf)
{
	t_route	*routes;
	t_link	*queue;
	int		flag;

	queue = NULL;
	routes = NULL;
	flag = 0;
	if (!ants_inf->start)
		error(ERR_N_19);
	ants_inf->start->visited = 1;
	push_back_link(&queue, ants_inf->start);
	push_back_link(&(ants_inf->start->path), ants_inf->start);
	while (bfs_search(ants_inf, room, &routes, &queue))
	{
		push_back_link(&queue, ants_inf->start);
		flag++;
	}
	if (!flag)
		error(ERR_N_18);
	ants_inf->num_of_routes = flag;
	free_links(&queue);
	return (routes);
}

/*
** @brief just as the name says.
** @param routes list of found routes.
** @param room list of rooms.
*/

void		free_everything(t_route **routes, t_room **room)
{
	free_rooms(room);
	free_routes(routes);
}

/*
** @brief checks start-end connection between rooms.
** @param room list of rooms.
** @param ants_inf basic info structure.
** @return 1 if there's such connection. Otherwise, 0.
*/

int			check_case_of_two_rooms(t_ant *ants_inf)
{
	t_link	*tmp;

	tmp = ants_inf->end->links;
	while (tmp)
	{
		if (tmp->room == ants_inf->start)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

/*
** @brief core of the program. Finds routes, send ants, frees what necessary
** etc.
** @param room list of rooms.
** @param ants_inf basin info structure.
** @param routes list of found routes.
*/

void		algo(t_room **room, t_ant *ants_inf, t_route **routes)
{
	char	*out;
	t_route	*shortest;

	if (!ants_inf->start || !ants_inf->end)
		error(ERR_N_19);
	if (check_case_of_two_rooms(ants_inf))
		return (two_rooms(ants_inf));
	(*routes) = find_routes(room, ants_inf);
	str_from_lsts(g_output, &out);
	ft_printf("%s\n", out);
	ft_lst_clear(&g_output);
	ft_strdel(&out);
	shortest = find_shortest_route(*routes);
	if (bonus_flags.p)
		output_paths(*routes, shortest);
	send_ants(routes, ants_inf, shortest);
	free_everything(routes, room);
}
