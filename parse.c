/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/24 16:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/24 16:57:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief used for processing ant's number and probable comments.
** @param ant structure which contains info about num of ants, also some
** additional info.
** @param line processed line by gnl.
** @return return 1 in case of comment input, 0 otherwise.
*/

int		process_the_first_line(t_ant *ant, char *line)
{
	int		tmp;

	if (line[0] == '#')
		return (1);
	else
	{
		tmp = ft_atoi(line);
		if ((ft_strlen(line) != (size_t)num_len(tmp)) || tmp <= 0)
			error(ERR_N_1);
		if (tmp > 10000)
			error(ERR_N_21);
		ant->num = tmp;
		ant->start = NULL;
		ant->end = NULL;
		ant->num_of_routes = 0;
		return (0);
	}
}

/*
** @brief checks whether retrieved room's parameters are correct.
** @param str_tmp room's name to be added to the list.
** @param tmp_x x coordinate.
** @param tmp_y y coordinate.
** @param room list of room's nodes.
*/

void	check_name_and_coords(char *str_tmp, int tmp_x, int tmp_y, t_room *room)
{
	while (room)
	{
		if (ft_strequ(str_tmp, room->name))
			error(ERR_N_8);
		if (tmp_x == room->coord_x && tmp_y == room->coord_y)
			error(ERR_N_9);
		room = room->next;
	}
}

/*
** @brief retrieves different params for room and fills rooms' list with
** a single node.
** @param room
** @param ant
** @param line
** @param flag
*/

void	check_and_fill_room(t_room **room, t_ant *ant, char *line, int *flag)
{
	int		tmp_x;
	int		tmp_y;
	char	*str_tmp;
	t_room	*tmp_room;

	str_tmp = retrieve_first_room_parameter(line);
	if (ft_strequ(str_tmp, "  "))
	{
		check_and_fill_link(room, ant, line);
		*flag = 42;
		return ;
	}
	tmp_x = retrieve_second_room_parameter(line);
	tmp_y = retrieve_third_room_parameter(line);
	check_name_and_coords(str_tmp, tmp_x, tmp_y, *room);
	tmp_room = push_back_to_room(room, str_tmp, tmp_x, tmp_y);
	if (*flag == 1)
		ant->start = tmp_room;
	else if (*flag == 2)
		ant->end = tmp_room;
	ft_strdel(&str_tmp);
	*flag = 0;
}

/*
** @brief parses the line.
** @details it comes into play after the first line with ants' number has been
** processed.
** @param room it's a list which will be filled with rooms.
** @param ant basic info structure to be filled.
** @param line line which is currently in processing.
*/

void	process_the_line(t_room **room, t_ant *ant, char *line)
{
	static int flag;

	if (flag == 42)
	{
		check_and_fill_link(room, ant, line);
		return ;
	}
	if (ft_strnequ(line, "##", 2))
	{
		commands_management(ant, &flag, line);
		return ;
	}
	else if (line[0] == '#')
		return ;
	if (line[0] == 'L')
		error(ERR_N_2);
	check_and_fill_room(room, ant, line, &flag);
}

/*
** @brief line processing happens here.
** @details Because first line contains number of ants, it processes it first.
**  Such division might be unnecessary, but it brings more control.
** @param room it's a list which will be filled with rooms.
** @param ant structure which contains general info.
*/

void	validate_and_stuff_it(t_room **room, t_ant *ant)
{
	char	*line;
	int		flag;
	int		ret;

	flag = 1;
	*room = NULL;
	while (flag && (ret = get_next_line(0, &line)))
	{
		if (ret == -1)
			error(ERR_N_17);
		lst_push_back_l(&g_output, line, ft_strlen(line));
		flag = process_the_first_line(ant, line);
		ft_memdel((void **)&line);
	}
	while ((ret = get_next_line(0, &line)))
	{
		if (ret == -1)
			error(ERR_N_17);
		lst_push_back_l(&g_output, line, ft_strlen(line));
		process_the_line(room, ant, line);
		ft_memdel((void **)&line);
	}
	if (!ant && (*room))
		error(ERR_N_19);
}
