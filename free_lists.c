/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_lists.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/08 16:24:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/08 16:24:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	free_links(t_link **alst)
{
	t_link *tmp;

	if (!alst || !(*alst))
		return ;
	while (*alst)
	{
		tmp = *alst;
		(*alst) = (*alst)->next;
		free(tmp);
	}
	*(alst) = NULL;
}

void	free_rooms(t_room **alst)
{
	t_room *tmp;

	if (!alst || !(*alst))
		return ;
	while (*alst)
	{
		tmp = *alst;
		ft_strdel(&(tmp->name));
		free_links(&(tmp->links));
		free_links(&(tmp->path));
		(*alst) = (*alst)->next;
		free(tmp);
	}
	*(alst) = NULL;
}

void	free_routes(t_route **alst)
{
	t_route *tmp;

	if (!alst || !(*alst))
		return ;
	while (*alst)
	{
		tmp = *alst;
		free_links(&(tmp->links));
		(*alst) = (*alst)->next;
		free(tmp);
	}
	*(alst) = NULL;
}
