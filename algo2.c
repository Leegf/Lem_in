/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/02 16:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/02 16:11:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief the name ;)
** @param new_link pointer to the room, which will contain newly merged path.
** @param parent old path, to which will be added new node.
*/

void	merge_lists(t_room *new_link, t_link *parent,
					t_link *links_iterat, t_link **queue)
{
	if (!new_link || !parent)
		return ;
	while (parent)
	{
		push_back_link(&(new_link->path), parent->room);
		parent = parent->next;
	}
	push_back_link(&(new_link->path), new_link);
	links_iterat->room->visited = 1;
	push_back_link(queue, links_iterat->room);
}

/*
** @brief name is self-explanatory.
** @param queue queue which is used for bfs' algo.
*/

void	pop_queue(t_link **queue)
{
	t_link *tmp;

	tmp = (*queue)->next;
	free(*queue);
	*queue = tmp;
}

/*
** @brief goes through the route's list and marks every nodes as 'used', so bfs
** won't add the same route twice.
** @param finish_room room which contains the whole path.
*/

void	mark_route_as_used(t_room *finish_room)
{
	t_link *tmp;

	tmp = finish_room->path;
	tmp = tmp->next;
	while (tmp->next)
	{
		tmp->room->used = 1;
		tmp = tmp->next;
	}
}

/*
** @brief rewinds changes caused by bfs' algo to the initial state.
** @param room list of rooms.
** @param ants_inf basic info structure.
*/

void	unmark_visited(t_room *room, t_ant *ants_inf)
{
	while (room)
	{
		if (room == ants_inf->start)
		{
			room = room->next;
			continue ;
		}
		room->visited = 0;
		if (room->path && !(room->used))
		{
			free_links(&(room->path));
			room->path = NULL;
		}
		room = room->next;
	}
}

/*
** @brief little function which handles adding new route to list of existing
** ones. Plus it makes some prep for next iteration.
** @param routes list of found routes.
** @param queue queue which is used for bfs' search.
** @param ants_inf basic info structure.
** @param room list of rooms.
** @return return 1 because of the norme. Doesn't have any reason beyond that.
*/

int		push_route_to_solutions(t_route **routes, t_link **queue,
								t_ant *ants_inf, t_room **room)
{
	push_back_route(routes, (*queue)->room->path);
	mark_route_as_used((*queue)->room);
	unmark_visited(*room, ants_inf);
	free(ants_inf->end->path);
	ants_inf->end->path = NULL;
	free_links(queue);
	return (1);
}
