/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 17:03:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/23 17:03:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief main, what could I say more?
**
** @return always returns 0. Errors are handled by exit.
*/

int		main(int ac, char **av)
{
	t_room		*room;
	t_ant		ant;
	t_route		*routes;

	routes = NULL;
	while (ac)
	{
		if (ft_strequ(av[ac - 1], "-p"))
			bonus_flags.p = 1;
		if (ft_strequ(av[ac - 1], "-s"))
			bonus_flags.s = 1;
		ac--;
	}
	validate_and_stuff_it(&room, &ant);
	algo(&room, &ant, &routes);
	return (0);
}
