/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 15:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/05/01 15:59:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_route		*new_route(t_link *links)
{
	t_route *tmp;
	int		size;

	size = 0;
	tmp = (t_route *)malloc(sizeof(t_route));
	if (tmp == NULL)
		return (NULL);
	if (links == NULL)
	{
		tmp->links = NULL;
		tmp->next = NULL;
	}
	else
	{
		tmp->links = NULL;
		while (links && ++size)
		{
			push_back_link(&(tmp->links), links->room);
			links = links->next;
		}
		tmp->next = NULL;
	}
	tmp->size = size - 2;
	return (tmp);
}

/*
** @brief pushed back the node with parameters to the back of the list.
** @param tmp ptr to the beginning of the list.
** @param links the thing to be added.
*/

void				push_back_route(t_route **tmp, t_link *links)
{
	t_route *kek;
	t_route *hm;

	if (!tmp)
		return ;
	kek = *tmp;
	hm = new_route(links);
	if (hm == NULL)
		return ;
	if (!(*tmp))
	{
		*tmp = hm;
		return ;
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
}
