/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_checking.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 12:19:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/26 12:19:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief see the name.
** @param line processed line.
** @return returns name of the room.
*/

char	*retrieve_first_room_parameter(char *line)
{
	char	*space_ptr;

	space_ptr = ft_strchr(line, ' ');
	if (!space_ptr)
	{
		if (ft_strchr(line, '-'))
			return ("  ");
		error(ERR_N_3);
	}
	return (ft_strsub(line, 0, (space_ptr - line)));
}

/*
** @brief see the name.
** @param line processed line.
** @return returns x coordinate.
*/

int		retrieve_second_room_parameter(char *line)
{
	char	*space_ptr;
	int		tmp;
	char	*tmp_ptr;

	space_ptr = ft_strchr(line, ' ');
	tmp_ptr = space_ptr + 1;
	tmp = ft_atoi(tmp_ptr);
	space_ptr = ft_strchr(tmp_ptr, ' ');
	if (!space_ptr)
		error(ERR_N_3);
	if (num_len(tmp) != (space_ptr - tmp_ptr))
		error(ERR_N_4);
	return (tmp);
}

/*
** @brief see the name.
** @param line processed line.
** @return returns y coordinate.
*/

int		retrieve_third_room_parameter(char *line)
{
	char	*space_ptr;
	int		tmp;
	char	*tmp_ptr;

	space_ptr = ft_strchr(line, ' ');
	tmp_ptr = space_ptr + 1;
	space_ptr = ft_strchr(tmp_ptr, ' ');
	tmp_ptr = space_ptr + 1;
	tmp = ft_atoi(tmp_ptr);
	if (num_len(tmp) != (line + (ft_strlen(line)) - tmp_ptr))
		error(ERR_N_4);
	return (tmp);
}
