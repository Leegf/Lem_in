/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 17:04:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/23 17:04:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft/includes/ft_printf.h"

t_list				*g_output;
struct s_fl			bonus_flags;

typedef struct		s_link
{
	struct s_room	*room;
	struct s_link	*next;
}					t_link;

typedef	struct		s_room
{
	char			*name;
	int				coord_x;
	int				coord_y;
	t_link			*links;
	t_link			*path;
	int				used;
	int				visited;
	struct s_room	*next;
}					t_room;

typedef struct		s_ant
{
	int				num;
	t_room			*start;
	t_room			*end;
	int				num_of_routes;
}					t_ant;

typedef struct		s_an
{
	int				num;
	t_link			*path;
}					t_an;

typedef struct		s_route
{
	t_link			*links;
	int				size;
	struct s_route	*next;
}					t_route;

typedef struct		s_fl
{
	int p;
	int s;
}					t_fl;

/*
** Functions made for parsing.
*/

void				validate_and_stuff_it(t_room **room, t_ant *ant);
char				*retrieve_first_room_parameter(char *line);
int					retrieve_second_room_parameter(char *line);
int					retrieve_third_room_parameter(char *line);
char				*retrieve_first_link_comp(char *line);
char				*retrieve_second_link_comp(char *line);
void				commands_management(t_ant *ant, int *flag, char *line);
void				check_and_fill_link(t_room **room, t_ant *ant, char *line);

/*
** Additional functions.
*/

void				error(char *str);
int					num_len(int num);
t_room				*push_back_to_room(t_room **tmp, char *name, int x, int y);
void				push_back_link(t_link **tmp, t_room *link);
void				push_back_route(t_route **tmp, t_link *links);
void				merge_lists(t_room *new_link, t_link *parent,
								t_link *links_iterat,
					t_link **queue);
void				free_links(t_link **alst);
void				lst_push_back_l(t_list **tmp, void *data, size_t con_s);
void				free_rooms(t_room **room);
void				free_routes(t_route **alst);
void				initialize_band(t_an ant_band[], int num);

/*
** Functions for algo.
*/

void				algo(t_room **room, t_ant *ants_inf, t_route **routes);
void				pop_queue(t_link **queue);
int					push_route_to_solutions(t_route **routes, t_link **queue,
								t_ant *ants_inf, t_room **room);
void				send_ants(t_route **routes, t_ant *ants_inf,
							t_route *shortest);
void				free_everything(t_route **routes, t_room **room);
void				process_ants(t_an ant_band[], t_ant *ants_inf, int count);
t_route				*find_shortest_route(t_route *routes);
void				two_rooms(t_ant *ants_inf);

/*
** bonus functions
*/

void				output_paths(t_route *routes, t_route *shortest);
void				send_via_shortest(t_ant *ants_inf, t_route *shortest);

/*
** There're lots of possible errors ;).
*/

# define ERR_N_0 "First line should contain the number of ants."
# define ERR_N_1 "Invalid number of ants. It should be a positive integer."
# define ERR_N_2 "'L' can't be the first letter of the room's name."
# define ERR_N_3 "Invalid room parameters."
# define ERR_N_4 "Either one of the room coordinates is not an integer."
# define ERR_N_5 "##start or ##end command should be followed by room par-s."
# define ERR_N_6 "There can't be more than one start."
# define ERR_N_7 "There can't be more than one end."
# define ERR_N_8 "There are rooms with the same name."
# define ERR_N_9 "There are two or more rooms which have the same coordinates."
# define ERR_N_10 "Wrong linking notation. There should be '-' between names."
# define ERR_N_11 "You have to link one room to the already existing one."
# define ERR_N_12 "You have to choose starting room. Type '##start' above it."
# define ERR_N_13 "You have to choose finishing room. Type '##end' above it."
# define ERR_N_14 "First room you're trying to link with does not exist."
# define ERR_N_15 "Second room you're trying to link with does not exist."
# define ERR_N_16 "You can't link a room to itself."
# define ERR_N_17 "Can't read from STDIN."
# define ERR_N_18 "There's no way to reach ending room."
# define ERR_N_19 "Don't ctrl+d me, pls ;)."
# define ERR_N_20 "Empty input."
# define ERR_N_21 "It's a bit too much for ants' number."

#endif
