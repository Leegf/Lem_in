/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_checking.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 15:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/04/26 15:07:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** @brief parsing func.
** @param line processed line.
** @return returns parsed parameter.
*/

char	*retrieve_first_link_comp(char *line)
{
	char	*hyphen_ptr;

	hyphen_ptr = ft_strchr(line, '-');
	if (!hyphen_ptr)
		error(ERR_N_10);
	return (ft_strsub(line, 0, (hyphen_ptr - line)));
}

/*
** @brief parsing func.
** @param line processed line.
** @return returns parsed parameter.
*/

char	*retrieve_second_link_comp(char *line)
{
	char	*hyphen_ptr;

	hyphen_ptr = ft_strchr(line, '-');
	return (ft_strsub(line, (hyphen_ptr - line) + 1,
					(line + ft_strlen(line) -
		hyphen_ptr)));
}
